import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
public class FIzzbuzzTest {
    @Test
    public void testFizz(){
        assertEquals("Fizz", Fizzbuzz.fizzBuzz(3));
    }

    @Test
    public void testBuzz(){
        assertEquals("Buzz", Fizzbuzz.fizzBuzz(5));
    }

    @Test
    public void testFizzBuzz(){
        assertEquals("FizzBuzz", Fizzbuzz.fizzBuzz(35));
    }
}
