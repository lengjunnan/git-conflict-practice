public class Fizzbuzz {
    public static String fizzBuzz(int num) {
        String res = "";
        String fizz = "Fizz";
        String buzz = "Buzz";
        String fizzbuzz = "FizzBuzz";
        String tmp = Integer.toString(num);

        if (num % 3 == 0 || tmp.contains("3")) {
            res += fizz;
        }

        if (num % 5 == 0 || tmp.contains("5")) {
            res += buzz;
        }

        return res.isEmpty() ? Integer.toString(num) : res;
    }
}
